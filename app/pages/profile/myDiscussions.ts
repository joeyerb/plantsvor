import {Page, NavController} from "ionic-angular/index";
import {DiscussionsPage} from "../common/common";

@Page({
  templateUrl: 'build/pages/profile/myDiscussions.html'
})
export class MyDiscussionsPage extends DiscussionsPage {
  
  constructor(nav: NavController) {
    super();
    this.nav = nav;
    this.getMyDiscussions();
  }
  
  getMyDiscussions() {
    let self = this;
    this.getDiscussions(false, 'user', () => {
      self.leftDiscussions = self.discussions.slice();
    });
  }
}