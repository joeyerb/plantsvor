import {Page, NavController, Modal} from 'ionic-angular';
import {GalleryPage} from "../common/gallery";
import {LoginModal} from "../common/login";
import {MyPostsPage} from "./myPosts";
import {MyDiscussionsPage} from "./myDiscussions";


@Page({
  templateUrl: 'build/pages/profile/profile.html'
})
export class ProfilePage {
  
  constructor(private nav: NavController) {}
  
  openGallery() {
    this.nav.push(GalleryPage, {wiki: false});
  }
  
  openLoginModal() {
    console.log('open modal');
    let modal = Modal.create(LoginModal);
    this.nav.present(modal);
  }
  
  openMyPosts() {
    this.nav.push(MyPostsPage);
  }
  
  openMyDiscussions() {
    this.nav.push(MyDiscussionsPage);
  }
}
