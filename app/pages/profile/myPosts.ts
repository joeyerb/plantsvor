import {Page, NavController} from "ionic-angular/index";
import {ListPage} from "../common/common";
import {flarum} from "../../flarum";

@Page({
  templateUrl: 'build/pages/profile/myPostsPage.html'
})

export class MyPostsPage extends ListPage {
  
  myName: string;
  posts: Array<any>;
  
  constructor(nav: NavController) {
    super();
    this.nav = nav;
    this.myName = flarum.username || 'Unknown';
    this.getMyPosts();
  }
  
  getMyPosts() {
    let self = this;
    this.auth(() => {
      flarum.getPosts('', flarum.userId, '', 'comment', resp => {
        self.myName = flarum.username;
        self.posts = resp.data;
        self.included = resp.included;
      }, err => {
        console.error(err);
      });
    });
  }
}