import {Page, NavController} from 'ionic-angular';
import {DiscussionsPage} from '../common/common';
import {AskDetailPage} from './askDetail';

@Page({
  templateUrl: 'build/pages/ask/ask.html'
})
export class AskPage extends DiscussionsPage {

  constructor(nav: NavController) {
    super();
    this.nav = nav;
    this.primaryTag = 'Ask';
    console.log('construct ' + this.primaryTag);
    let self = this;
    this.getDiscussions(false, 'tag', () => {
      self.leftDiscussions = self.discussions.slice();
    });
  }

  openDetail(item: any) {
    console.log('clicked', item);
    this.nav.push(AskDetailPage, {discussion: item});
  }
  
}
