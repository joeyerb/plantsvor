import {Page, NavParams, NavController} from 'ionic-angular';
import {PostsPage} from "../common/common"

@Page({
  templateUrl: 'build/pages/ask/askDetail.html'
})
export class AskDetailPage extends PostsPage {
  constructor(nav: NavController, params: NavParams) {
    super(nav, params);
  }
}
