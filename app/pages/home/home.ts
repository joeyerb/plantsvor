import {Page, NavController} from 'ionic-angular';
import {Http} from 'angular2/http';
import 'rxjs/add/operator/toPromise'
import {DiscussionsPage} from "../common/common";
import {FResource} from "../../flarum";
import {AskDetailPage} from "../ask/askDetail";
import has = Reflect.has;
import {ShareDetailPage} from "../share/shareDetail";
import {MapsPage} from "../wiki/maps";
import {Geolocation} from 'ionic-native';

interface Iaqi {
  p: string,
  v: number[],
  i: string,
  h: any
}

@Page({
  templateUrl: 'build/pages/home/home.html'
})
export class HomePage extends DiscussionsPage {
  
  aqi_url: string = 'https://k3juz6rcti.execute-api.ap-northeast-1.amazonaws.com/dev/hangzhou';
  city: any;
  iaqi: Iaqi[];
  position: any;

  constructor(nav: NavController, private http: Http) {
    super();
    this.nav = nav;
    this.getIaqi();
    this.getNewDiscussions();
    this.getLocation();
  }

  getIaqi() {
    var self = this;
    this.http.get(this.aqi_url)
      .toPromise().then(resp => {
      var data = resp.json();
      console.log('got data', typeof data, data.city);
      self.city = data.city;
      self.iaqi = data.iaqi;
    }).catch(err => {
      console.error(err);
    });
  }
  
  getNewDiscussions() {
    let self = this;
    this.getDiscussions(true, 'tag', () => {
      self.leftDiscussions = self.discussions.slice();
    });
  }

  openDetail(item: FResource) {
    let self = this;
    let hasTag = function (tag_name) {
      return item.relationships.tags.data.findIndex(tag => {
          let _tag = self.getResource('tags', tag.id);
          if (_tag) {
            //console.log('tag is', tag.id, _tag.attributes.name);
            return _tag.attributes.name === tag_name;
          }
        }) >= 0;
    };
    if (hasTag('Ask')) {
      console.log('Opening Ask page');
      this.nav.push(AskDetailPage, {discussion: item});
    } else if (hasTag('Share')) {
      console.log('Opening Share page');
      this.nav.push(ShareDetailPage, {discussion: item});
    }


  }

  openMaps() {
    this.nav.push(MapsPage, {position: this.position});
  }

  getLocation() {
    console.log('Get location ...............');
    Geolocation.getCurrentPosition().then(position => {
      console.log('got position:', position.coords.longitude + ' ' + position.coords.latitude);
      this.position = position;
    }).catch(err => {
      console.error(err);
    });

    // var subscription = Geolocation.watchPosition().subscribe(position => {
    //   console.log(position.coords.longitude + ' ' + position.coords.latitude);
    // });

    // subscription.unsubscribe();
  }

  
}
