import {Page, NavController, NavParams} from 'ionic-angular';
import {PostsPage} from "../common/common";

@Page({
  templateUrl: 'build/pages/share/shareDetail.html'
})
export class ShareDetailPage extends PostsPage {
  constructor(nav: NavController, params: NavParams) {
    super(nav, params);
  }
}
