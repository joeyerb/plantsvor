import {Page, NavController} from 'ionic-angular';
import {DiscussionsPage} from '../common/common';
import * as moment from 'moment'
import {ShareDetailPage} from "./shareDetail";

@Page({
  templateUrl: 'build/pages/share/share.html',
})
export class DiscussionPage extends DiscussionsPage {
  constructor(nav: NavController) {
    super();
    this.nav = nav;
    this.primaryTag = 'Share';
    console.log('construct ' + this.primaryTag);
    let self = this;
    this.getDiscussions(false, 'tag', () => {
      self.leftDiscussions = self.discussions.slice();
    });
  }

  getDuration(datetime: string) {
    var dur = moment().diff(moment(datetime));
    return moment.duration(dur).humanize();
  }
  
  openDetail(item: any) {
    this.nav.push(ShareDetailPage, {discussion: item});
  }
}
