import {ViewController, Toast, NavController, Page, NavParams} from "ionic-angular/index";
import {flarum} from "../../flarum";
import {RegisterPage} from "./register";

@Page({
  templateUrl: 'build/pages/common/login.html'
})
export class LoginModal {
  
  username: string = "admin";
  password: string = "@iE3>)o3GG'T";
  callback: Function;
  
  constructor(
    params: NavParams,
    private nav: NavController,
    private viewCtrl: ViewController) {
    this.callback = params.get('callback');
  }
  
  close() {
    this.viewCtrl.dismiss();
  }
  
  doLogin() {
    if (!this.username || !this.password) {
      let toast = Toast.create({
        message: 'Username and password cannot be empty.',
        duration: 2000
      });
      this.nav.present(toast);
    }
    flarum.setUser(this.username, this.password, resp => {
      console.log('Login done.', resp);
      this.callback();
      this.close();
    }, err => {
      console.error(err);
      let toast = Toast.create({
        message: 'Login failed.',
        duration: 3000
      });
      this.nav.present(toast);
    });
  }
  
  openRegisterPage() {
    this.nav.push(RegisterPage);
  }
}

