import {Page, NavParams, NavController, Alert} from 'ionic-angular';
import {flarum} from "../../flarum";
import {DiscussionsPage} from "./common";
import {ImagePicker, Transfer} from "ionic-native"
import {FileUploadOptions} from "ionic-native/dist/plugins/filetransfer";

@Page({
  templateUrl: 'build/pages/common/createDiscussion.html'
})
export class CreateDiscussionPage {
  testpng: string = "img/tree.jpg";
  parent: DiscussionsPage;
  discussionType: string;
  nav: NavController;
  
  shareData: any = {
    title: '',
    content: ''
  };
  tagChecker: any = {
    Tree: false,
    Flower: false,
    Photo: false
  };
  selectedTags: string[] = [];

  photoUris: string[] = [];

  constructor(nav: NavController, params: NavParams) {
    this.nav = nav;
    this.parent = params.get('parent');
    this.discussionType = params.get('type');
  }

  tagAlert() {
    let alert = Alert.create({
      title: 'Select tags',
      buttons: ['Cancel', {
        text: 'OK',
        handler: data => {
          this.selectedTags = data;
          for (let tag in this.tagChecker) {
            if (this.tagChecker.hasOwnProperty(tag))
              this.tagChecker[tag] = data.indexOf(tag) >= 0;
          }
          console.log('check data:', data);
          console.log(this.tagChecker);
        }
      }]
    });
    for (let tag in this.tagChecker) {
      if (this.tagChecker.hasOwnProperty(tag)) {
        alert.addInput({
          type: 'checkbox',
          label: tag,
          value: tag,
          checked: this.tagChecker[tag]
        });
      }
    }
    this.nav.present(alert);
  }

  postData() {
    var self = this;
    var discussionTypes = ['Share', 'Ask'];
    if (discussionTypes.indexOf(this.discussionType) < 0) {
      console.error('Wrong type:', this.discussionType);
      return; // TODO
    }
    var tags = [this.discussionType];
    for (let tag in self.tagChecker) {
      if ( self.tagChecker.hasOwnProperty(tag) && self.tagChecker[tag] ) {
        tags.push(tag);
      }
    }
    console.log('tags:', tags);
    flarum.createDiscussion(this.shareData, tags, resp => {
      self.parent.receiveData(resp.data, resp.included);
      self.nav.pop();
      // TODO refresh
    }, undefined);
  }

  pickPhoto() {
    var self = this;
    ImagePicker.getPictures({
      maximumImagesCount: 3
    }).then(results => {
      results.forEach(res => {
        console.log("Uri:" + res);
      });
      self.photoUris = results;
      self.uploadPhoto();
    })

  }

  uploadPhoto() {
    if (this.photoUris.length == 0) {
      console.error('no photo selected');
      return;
    }
    var self = this;
    // TODO user
    var username = 'admin';
    var uploadUrl = "https://pl.zjut.party/api/upload/" + username;
    const fileTransfer = new Transfer();

    fileTransfer.onProgress((progressEvent: ProgressEvent) => {
      if (progressEvent.lengthComputable) {
        console.log("loaded: " + progressEvent.loaded);
        console.log("all: " + progressEvent.total);
      }
    });
    this.photoUris.forEach(uri => {
      console.log('uploading: ' + uri);

      let options = new FileUploadOptions();
      options.fileKey = "file";
      options.fileName = uri.substr(uri.lastIndexOf('/') + 1);
      options.mimeType = "image/jpeg";

      fileTransfer.upload(uri, encodeURI(uploadUrl), options).then(resp => {
        console.log(resp.headers + resp.response);
        let imgsrc = JSON.parse(resp.response).path;
        self.shareData.content = '[img]' + imgsrc + '[/img]\n' + self.shareData.content;
      }).catch(err => {
        console.error('error:' + JSON.stringify(err));
      })
    })
  }
}
