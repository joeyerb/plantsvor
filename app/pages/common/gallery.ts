import {Page, NavParams} from 'ionic-angular'
import {ListPage} from "./common";
import {Http} from "angular2/http";
import {flarum} from "../../flarum";

@Page({
  templateUrl: 'build/pages/common/gallery.html'
})
export class GalleryPage extends ListPage {

  apiUrl: string;
  username: string;

  baseUrl: string = '';
  photos: string[] = [];
  photos2: Array[] = [];

  constructor(private http: Http, params: NavParams) {
    super();
    if (params.get('wiki')) {
      let photoObj = params.get('photos');
      this.setPhotos(photoObj);
    } else {
      this.apiUrl = 'https://pl.zjut.party/api/photos/';
      this.username = flarum.username;
      this.getPhotos();
    }
  }

  getPhotos() {
    console.log('loading photos');
    let self = this;
    self.http
      .get(self.apiUrl + self.username)
      .toPromise().then(resp => {
        self.setPhotos(resp.json());
      });
  }
  
  setPhotos(data) {
    let self = this;
    self.baseUrl = data.path;
    self.photos = data.photos;
    for (let i=0, l=self.photos.length; i<l; i+=2) {
      if (i==l-1) {
        self.photos2.push([self.baseUrl + self.photos[i]]);
      } else {
        self.photos2.push([self.baseUrl + self.photos[i], self.baseUrl + self.photos[i+1]]);
      }
    }
  }
  
  sharePhotos() {
    // TODO
  }
  
}