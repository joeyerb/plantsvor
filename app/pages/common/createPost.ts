import {Page, NavParams, NavController, Alert} from 'ionic-angular';
import {flarum} from "../../flarum";
import {AskDetailPage} from "../ask/askDetail";

@Page({
  templateUrl: 'build/pages/common/createPost.html'
})
export class CreatePostPage {
  nav: NavController;
  parent: AskDetailPage;
  discussionID: string;
  content: string = '';
  
  constructor(nav: NavController, params: NavParams) {
    this.nav = nav;
    this.discussionID = params.get('discussionID');
    this.parent = params.get('parent');
  }
  
  postData() {
    var self = this;
    console.log(this.content);
    flarum.createPost(this.discussionID, this.content, resp => {
      self.parent.receiveData(resp.data, resp.included);
      self.nav.pop();
    }, undefined);
  }
}
