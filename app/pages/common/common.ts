import {flarum, FResource} from '../../flarum';
import {NavController, Alert, NavParams, Modal} from "ionic-angular/index";
import {CreateDiscussionPage} from "./createDiscussion";
import {CreatePostPage} from "./createPost";
import {LoginModal} from "./login";

export class ListPage {

  included: any[] = [];
  nav: NavController;

  constructor() {}

  getResource(type: string, id: string) {
    return this.included.find((obj: FResource) => {
      return obj.type === type && obj.id === id;
    });
  }

  getRandom() {
    return Math.random() * 16 | 0;
  }

  tuneTime(timeStr: string) {
    let _tm = (new Date(timeStr)).toString();
    return _tm.substring(_tm.indexOf(' ') + 1, _tm.lastIndexOf(':'));
    //return timeStr.replace('T', ' ').replace(/\+.*/, '');
  }
  
  /**
   * Authorization
   * @param callback
   */
  auth(callback: Function) {
    if (flarum.authed) {
      console.log('Already logged in.');
      callback();
    } else {
      console.log('Try to login.');
      let modal = Modal.create(LoginModal, {callback: callback});
      this.nav.present(modal);
    }
  }

}

export class DiscussionsPage extends ListPage {
  constructor() {
    super();
  }

  discussions: any[] = [];
  leftDiscussions: any[];
  primaryTag: string = '';
  queryString: string;

  /**
   * Open a CreateDiscussion Page
   * @param discussionType
   */
  openCreatePage(discussionType: string) {
    this.nav.push(CreateDiscussionPage, {parent: this, type: discussionType});
  }

  /**
   * Receive data from next page
   * @param newDiscussion
   * @param newIncluded
   */
  receiveData(newDiscussion: FResource, newIncluded: any) {
    newIncluded.forEach(newIn => this.included.unshift(newIn));
    this.extractTags(newDiscussion);
    this.discussions.unshift(newDiscussion);
  }

  // TODO
  // reloadDiscussions() { }

  /**
   * Get discussions
   * //@param page TODO
   */
  getDiscussions(autoLogin: boolean, queryType: string, callback: Function) {
    let self = this;
    let doGet = function() {
      let query = '';
      if (queryType === 'tag') {
        if (self.primaryTag) {
          query = 'tag:' + self.primaryTag;
        }
      }
      if (queryType === 'user') {
        query = 'author:' + flarum.username;
      }
      flarum.getDiscussions(query, (resp) => {
        self.discussions = resp.data;
        self.included = resp.included;
        console.log('Discussions', resp);
        self.discussions.forEach(discussion => {
          discussion.attributes.likes = self.getRandom();
          self.extractTags(discussion);
        });
        callback();
        if (autoLogin) flarum.authed = false;
      }, undefined);
    };
    if (autoLogin) {
      flarum.setUser('test', 'testtest', resp => {
        console.log('Test login done.', resp);
        doGet();
      }, err => {
        console.error(err);
      });
    } else {
      this.auth(doGet);
    }
  }

  /**
   * Refresh items
   */
  doRefresh(refresher) {
    let self = this;
    this.getDiscussions(false, 'tag', () => {
      console.log('refresh done');
      self.leftDiscussions = self.discussions.slice();
      refresher.complete();
    });
    setTimeout(() => {
      console.log('refresh timeout');
      refresher.complete();
    }, 5000);
  }


  /**
   * Filter discussions by queryString
   */
  filterDiscussions() {
    if (!this.queryString) {
      this.leftDiscussions = this.discussions.slice();
    }
    var self = this;
    this.leftDiscussions = this.discussions.filter(discussion => {
      return discussion.attributes.title.indexOf(self.queryString) >= 0;
    });
  }

  /**
   * Extract tags from relationships property
   * @param discussion
   */
  protected extractTags(discussion: any) {
    let self = this;
    discussion.tags = [];
    discussion.relationships.tags.data.forEach(tag => {
      let _tag = self.getResource('tags', tag.id);
      if (_tag.attributes.name !== self.primaryTag)
        discussion.tags.push(_tag);
    });
  }

  /**
   * Remove a discussion
   * @param discussionID
   */
  protected removeDeletedItem(discussionID: string) {
    this.auth(() => {
      flarum.deleteDiscussion(discussionID, resp => {
        let delIndex = this.discussions.findIndex(discussion => {
          return discussion.id == discussionID;
        });
        if (delIndex >= 0)
          this.discussions.splice(delIndex, 1);
        else
          console.error('No such item:', discussionID);
      }, undefined);
    });
  }

  /**
   * Alert about deleting discussion
   * @param discussionID
   */
  deletingAlert(discussionID: string) {
    let alert = Alert.create({
      title: "Delete this?",
      message: "",
      buttons: ['Cancel', {
        text: 'OK',
        handler: () => {
          this.removeDeletedItem(discussionID);
        }
      }]
    });
    this.nav.present(alert);
  }
  
}

export class PostsPage extends ListPage {
  discussion: any;
  posts: any[] = [];

  constructor(nav: NavController, params: NavParams) {
    super();
    this.nav = nav;
    this.discussion = params.get('discussion');
    this.getPosts();
    console.log('init detail page');
  }

  getPosts() {
    var self = this;
    flarum.getDiscussionById(this.discussion.id, resp => {
      self.discussion = resp.data;
      self.included = resp.included;
      self.posts = resp.data.relationships.posts.data;
      console.log(self.posts);
      for (var i=self.posts.length-1; i>=0; i--) {
        var post = self.posts[i];
        var tmpPost = self.getResource('posts', post.id);
        if (tmpPost.attributes.contentType === 'comment') {
          tmpPost.relationships.user.data = self.getResource('users',
            tmpPost.relationships.user.data.id);
          self.posts[i] = tmpPost;
        } else {
          // Remove other types
          self.posts.splice(i, 1);
        }
      }
    }, undefined);
  }

  openPostPage() {
    this.nav.push(CreatePostPage, {discussionID: this.discussion.id, parent: this});
  }

  receiveData(newPost: FResource, newIncluded: any[]) {
    newIncluded.forEach(newIn => this.included.unshift(newIn));
    newPost.relationships.user.data = this.getResource('users',
      newPost.relationships.user.data.id);
    this.posts.push(newPost);
  }

}
