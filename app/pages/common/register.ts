import {Page, NavController, Toast} from "ionic-angular/index";
import {flarum} from "../../flarum";

@Page({
  templateUrl: 'build/pages/common/register.html'
})
export class RegisterPage {

  username: string;
  email: string;
  password: string;
  password2: string;

  constructor(
    private nav: NavController
  ) {}

  close() {
    this.nav.pop();
  }
  
  reset() {
    this.username = '';
    this.email = '';
    this.password = '';
    this.password2 = '';
  }

  private showToast(msg: string) {
    let toast = Toast.create({
      message: msg,
      duration: 3000
    });
    this.nav.present(toast);
  }

  doRegister() {
    if (this.password != this.password2) {
      this.showToast('Password not match.');
      return;
    }
    if (this.password.length < 8) {
      this.showToast('Password is too short (at least 8 characters).');
      return;
    }
    flarum.createUser(this.username, this.email,
      this.password, resp => {
        console.log('Register done.');
        this.close();
      }, err => {
        console.error(err);
        let errObj = JSON.parse(err.msg), errMsg: string = '';
        if (errObj.errors.length)
            errMsg = '\n' + errObj.errors[0].detail;
        this.showToast('Register failed.' + errMsg);
      })
  }
}
