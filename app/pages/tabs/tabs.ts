import {Page, Tabs} from 'ionic-angular';
import {HomePage} from '../home/home';
import {WikiPage} from '../wiki/wiki';
import {DiscussionPage} from '../share/share';
import {AskPage} from '../ask/ask';
import {ProfilePage} from '../profile/profile';
import {ViewChild} from "angular2/core";


@Page({
  templateUrl: 'build/pages/tabs/tabs.html'
})
export class TabsPage {
  homeRoot: any = HomePage;
  wikiRoot: any = WikiPage;
  shareRoot: any = DiscussionPage;
  askRoot: any = AskPage;
  profileRoot: any = ProfilePage;

  @ViewChild('myTabs') tabRef: Tabs;
  
  selectTab(idx: number) {
    this.tabRef.select(idx);
  }
}
