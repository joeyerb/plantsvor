import {Page, NavController, ViewController, Modal, NavParams, Toast} from 'ionic-angular';
import {Http} from 'angular2/http'
import {GalleryPage} from "../common/gallery";


interface WikiData {
  name: string,
  name_zh: string,
  title: string,
  reference: string,
  description: string,
  img_title: string,
  img_src: string,
  taxa: Array<any>,
  photos: any,
  page: string,
}

@Page({
  templateUrl: 'build/pages/wiki/wiki.html'
})
export class WikiPage {
  
  plantName: string;
  shouldHideCancel: boolean = false;
  
  data: WikiData = {
    name: '',
    name_zh: '',
    title: '',
    reference: '',
    description: '',
    img_title: '',
    img_src: '',
    taxa: [],
    photos: null,
    page: '',
  };
  taxa0: string = '';

  photos: Array<string> = [];
  baseUrl: string = '';

  constructor(private http: Http, private nav: NavController) {
    this.plantName = 'Malus pumila';
    this.searchWiki();
  }

  private fillThumbs(data) {
    this.baseUrl = data.path;
    this.photos = data.photos.slice(0, 4);
  }

  private fillTaxa(taxa) {
    if (taxa.length > 0) {
      this.taxa0 = taxa[0].name;
      for (let i=0; i<taxa.length; i++) {
        if (taxa[i].link) {
          this.taxa0 = taxa[i].name;
          break;
        }
      }
    }
  }

  searchWiki() {
    this.shouldHideCancel = true;
    let self = this;
    this.http.get('https://pl.zjut.party/api/wiki/' + this.plantName + '?format=json')
      .toPromise()
      .then(resp => {
        self.data = resp.json();
        self.fillThumbs(self.data.photos);
        self.fillTaxa(self.data.taxa)
      }).catch(err => {
        console.error('wiki loading error:', err);
    });
  }

  pushTaxaList() {
    let modal = Modal.create(TaxaModal, {taxa: this.data.taxa, parent: this});
    this.nav.present(modal);
  }
  
  openGallery() {
    this.nav.push(GalleryPage, {wiki: true, photos: this.data.photos})
  }

}

@Page({
  template: `
  <ion-toolbar>
      <ion-title>
          Scientific classification
      </ion-title>
      <ion-buttons end>
          <button (click)="close()">
              <ion-icon name="close-circle"></ion-icon>
          </button>
      </ion-buttons>
  </ion-toolbar>
  <ion-content padding>
    <ion-list>
      <button ion-item *ngFor="#k of taxa" (click)="searchItem(k)">{{ k.name }}</button>
    </ion-list>
  </ion-content>`
})
class TaxaModal {
  taxa: Array<any> = [];
  parent: any;
  
  constructor(
    private viewCtrl: ViewController,
    private nav: NavController,
    params: NavParams
  ) {
    this.taxa = params.get('taxa');
    this.parent = params.get('parent');
  }

  close() {
    this.viewCtrl.dismiss();
  }
  
  searchItem(item) {
    if (item.link) {
      this.parent.plantName = item.link;
      this.parent.searchWiki();
    } else {
      let toast = Toast.create({
        message: 'No content found for ' + item.name,
        duration: 3000
      });
      this.nav.present(toast);
    }
    this.close();
  }
}
