import {Page, NavParams} from 'ionic-angular'
// import {GoogleMap, GoogleMapsEvent} from "ionic-native/dist/index";
import {Geolocation} from "ionic-native"

@Page({
  templateUrl: 'build/pages/wiki/maps.html'
})
export class MapsPage {

  map: any;
  pos: any;

  constructor(params: NavParams) {
    this.map = null;
    this.pos = params.get('position');
  }

  ngOnInit(){
    this.loadMap();
    // this.map = new GoogleMap('map');
    // this.map.on(GoogleMapsEvent.MAP_READY).subscribe(() => console.log("Map is ready!"));
  }

  loadMap(){

    let lat = 30.2485736, lng = 120.1437163;
    if (this.pos) {
      lat = this.pos.coords.latitude;
      lng = this.pos.coords.longitude;
    }
      
    //noinspection TypeScriptUnresolvedVariable
    let latLng = new google.maps.LatLng(lat, lng);

    //noinspection TypeScriptUnresolvedVariable
    let mapOptions = {
      center: latLng,
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    //noinspection TypeScriptUnresolvedVariable
    this.map = new google.maps.Map(document.getElementById("map"), mapOptions);

  }

  locate() {
    Geolocation.getCurrentPosition().then(resp => {
      this.map.setCenter({lat: resp.coords.latitude, lng: resp.coords.longitude});
    }).catch(err => {
      console.error(err);
    });
  }

  // TODO
  addMark() {}

}