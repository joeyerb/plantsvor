/*jshint node:true, unused:false */

class Flarum {
  public userId: string;
  public username: string;
  public authed: boolean = false;

  private site: string;
  private password: string;
  private token: string = '';
  private allTags: any[] = [];
  private debugOn: boolean = true;

  constructor(_site) {
    this.site = _site;
  }
  
  setUser(_username, _password, onSuccess, onFailure) {
    if (this.username === _username && this.password === _password) {
      if (!this.token) {
        this.auth(onSuccess, onFailure);
      } else {
        onSuccess('Already in.')
      }
    } else {
      this.username = _username || '';
      this.password = _password || '';
      this.auth(onSuccess, onFailure);
    }
  }
  
  auth(onSuccess, onFailure) {
    if (!this.username || !this.password) {
      console.error('Invalid username or password!');
      return;
    }
    var user = "identification=" + this.username + "&password=" + this.password;
    var self = this;
    this.apiRequest('POST', '/api/token', user, function(resp) {
      self.token = resp.token;
      self.userId = resp.userId;
      self.authed = true;
      console.log("Authed");
      onSuccess(resp);
    }, onFailure);
  }


  // Request
  apiRequest(method, path, params, onSuccess, onFailure) {
    if (typeof(onFailure) !== 'function') {
      onFailure = function(err) {
        console.error("ERROR:", JSON.stringify(err));
      };
    }
    if (!this.site) {
      onFailure({"msg": "site not set"});
      return;
    }

    var xhr = new XMLHttpRequest();

    xhr.onreadystatechange = function() {
      //if (debugOn)
      //    console.log('http ready: ' + xhr.readyState);
      if (xhr.readyState == 4) {
        if (this.debugOn) console.log('http status: ' + xhr.status);
        if (xhr.status == 200 || xhr.status == 201) {
          var resp = JSON.parse(xhr.responseText);
          if (typeof(onSuccess) === 'function') {
            onSuccess(resp);
          } else {
            onFailure({"msg": "onSuccess is not a function"});
          }
        } else if (xhr.status == 204) {
          if (this.debugOn) console.log('Resource deleted');
          if (typeof(onSuccess) === 'function') {
            onSuccess({'msg': "resource deleted"})
          } else {
            onFailure({"msg": "onSuccess is not a function"});
          }
        } else if (xhr.status == 0) {
          onFailure({"msg": "network error"});
        } else {
          onFailure({"msg": xhr.responseText});
        }
      }
    };

    xhr.timeout = 4000;
    xhr.ontimeout = function() {
      if (this.debugOn) console.warn('Request timeout.');
    };

    if (this.token || path === '/api/token' ||
        (path === '/api/users' && method === 'POST')) {
      if (this.debugOn) console.log('Sending request A');
      this._sendRequest(xhr, method, path, params);
    } else {
      var self = this;
      if (this.debugOn) console.log('\nAquiring auth');
      this.auth(function(resp) {
        if (self.debugOn) console.log('Sending request B');
        if (resp) this._sendRequest(xhr, method, path, params);
      }, function(err) {
        console.error(err);
        onFailure({"msg": "auth failed"});
      });
    }
  }

  _sendRequest(xhr, method, path, params) {
    if (method === 'POST' || method === 'PATCH') {
      // POST, PATCH
      //method = 'POST';    // PATCH is not supported FIXME
      if (this.debugOn) console.log(method, this._genUrl(path, ''));
      xhr.open(method, this._genUrl(path, ''), true);
      if (path === '/api/token') {
        // Authorization
        if (this.debugOn) console.log('content type: urlencoded');
        xhr.setRequestHeader('Content-Type',
                             'application/x-www-form-urlencoded');
      } else if (method === 'POST' && path === '/api/users') {
        // User register
        xhr.setRequestHeader('Content-Type',
                             'application/json; charset=utf-8');
      } else if (this.token) {
        // Other POSTs
        if (this.debugOn) console.log('add token', this.token);
        xhr.setRequestHeader('Content-Type',
                             'application/json; charset=utf-8');
                             xhr.setRequestHeader('Authorization', 'Token ' + this.token);
      } else {
        console.error('Invalid request.');
      }
      if (this.debugOn) console.log('data:', params);
      xhr.send(params);
    } else {
      // GET, DELETE,
      if (this.debugOn) console.log(method, this._genUrl(path, params));
      xhr.open(method, this._genUrl(path, params), true);
      if (this.token) {
        if (this.debugOn) console.log('add token', this.token);
        xhr.setRequestHeader('Authorization', 'Token ' + this.token);
      }
      xhr.send();
    }
  }

  _genUrl(path, params) {
    path = path || '';
    params = params || '';
    if (path.indexOf('/') !== 0) {
      path = '/' + path;
    }
    if (params && params.indexOf('?') < 0) {
      params = '?' + params;
    }
    return this.site + path + params;
  }


  static _genData(type, id, attributes, relationships) {
    var data = {
      "id": undefined,
      "type": type,
      "attributes": attributes,
      "relationships": undefined
    };
    if (id !== '') {
      data.id = id;
    }
    if (relationships) {
      data.relationships = relationships;
    }
    return JSON.stringify({"data": data});
  }


  // _Tags
  _getTags(callback) {
    var self = this;
    this.getSiteInfo(function(resp: any) {
      self.allTags = [];
      var _included = resp.included;
      for (var i=0; i<_included.length; i++) {
        if (_included[i].type === 'tags') {
          if (self.debugOn)
            console.log('found tag:', _included[i].attributes.name);
          var _i = _included[i].id;
          self.allTags[_i] = _included[i].attributes;
        }
      }
      callback();
    }, undefined);
  }
  _getTagId(tagName) {
    for (var i=0; i<this.allTags.length; i++) {
      if (this.allTags[i] && this.allTags[i].name === tagName) {
        return i;
      }
    }
  }
  _genTags(tags, relationships) {
    for (var i=0; i<tags.length; i++) {
      relationships.tags.data.push({
        "type": "tags",
        "id": this._getTagId(tags[i])
      });
    }
    return relationships;
  };


  // Forum
  /**
   * GET /api/forum
   *  - get information about the forum, including groups and tags
   */
  getSiteInfo(onSuccess, onFailure) {
    this.apiRequest('GET', '/api/forum', '', onSuccess, onFailure);
  }

  /**
   * PATCH /api/forum - update forum config
   *
   *   TODO
   */
  setSiteInfo(onSuccess, onFailure) {
    var data = '';
    this.apiRequest('PATCH', '/api/forum', data, onSuccess, onFailure);
  }


  // Discussions
  /**
   * GET /api/discussions
   *  - get all discussions (sort is -time by default)
   *
   *     filter[q] - filter by username/gambits
   */
  getDiscussions(q, onSuccess, onFailure) {
    var params = '';
    if (q) { params += 'filter[q]=' + q; }
    this.apiRequest('GET', '/api/discussions', params, onSuccess, onFailure);
  }

  /**
   * POST /api/discussions - create a new discussion
   *
   *    - data: { title, content, tags }
   *      - title: string, discussion title
   *      - content: string, post content
   *      - tags: string array
   */
  createDiscussion(data, tags, onSuccess, onFailure) {
    var self = this;
    var attributes = {"title": data.title, "content": data.content};
    var relationships = {"tags": {"data": []}};
    var _apiRequest = function() {
      for (var i=0; i<tags.length; i++) {
        relationships.tags.data.push({
          "type": "tags",
          "id": self._getTagId(tags[i])
        });
      }
      relationships = self._genTags(tags, relationships);
      var dataStr = Flarum._genData('discussions', '', attributes,
                             relationships);
      self.apiRequest('POST', '/api/discussions', dataStr, onSuccess, onFailure);
    };
    if (this.allTags.length === 0) {
      this._getTags(_apiRequest);
    } else {
      _apiRequest();
    }
  }

  /**
   * GET /api/discussions/:id - get a discussion by ID
   */
  getDiscussionById(discussionId, onSuccess, onFailure) {
    this.apiRequest('GET', '/api/discussions/' + discussionId, '',
               onSuccess, onFailure);
  }

  /**
   * PATCH /api/discussions/:id - update a discussion
   *
   *    - discussionId: int
   *    - title: string, discussion title
   *    - tags: string array
   */
  updateDiscussion(discussionId, title, tags, onSuccess, onFailure) {
    var path = '/api/discussions/' + discussionId;
    var attributes, relationships;
    var _apiRequest = function() {
      if (tags) relationships = this._genTags(tags, relationships);
      var dataStr = Flarum._genData('discussions', discussionId,
                             attributes, relationships);
      this.apiRequest('PATCH', path, dataStr, onSuccess, onFailure);
    };
    if (title) attributes = {"title": title};
    if (tags) {
      relationships = {"tags": {"data": []}};
      if (this.allTags.length === 0) {
        this._getTags(_apiRequest);
      } else {
        _apiRequest();
      }
    } else {
      _apiRequest();
    }
  }
  /**
   * PATCH /api/discussions/:id - follow a discussion
   *
   *    - discussionId: int
   *    - follow: bool
   */
  followDiscussion(discussionId, follow, onSuccess, onFailure) {
    var path = '/api/discussions/' + discussionId;
    var attributes;
    if (follow) {
      attributes = {"subscription": "follow"};
    } else {
      attributes = {"subscription": false};
    }
    var dataStr = Flarum._genData('discussions', discussionId, attributes, undefined);
    this.apiRequest('PATCH', path, dataStr, onSuccess, onFailure);
  }

  /**
   * DELETE /api/discussions/:id - delete a discussion
   */
  deleteDiscussion(discussionId, onSuccess, onFailure) {
    var path = '/api/discussions/' + discussionId;
    this.apiRequest('DELETE', path, '', onSuccess, onFailure);
  }


  // Posts
  /**
   * GET /api/posts - get all posts
   *
   *     filter[discussion] - filter by discussion ID
   *     filter[user] - filter by user ID
   *     filter[number]
   *          - filter by number (position within the discussion)
   *     filter[type] - filter by post type
   */
  getPosts(discussionId, userId, number, type, onSuccess,
      onFailure) {
    var params = '';
    params += discussionId ? '&filter[discussion]=' + discussionId : '';
    params += userId ? '&filter[user]=' + userId : '';
    params += number ? '&filter[number]=' + number : '';
    params += type ? '&filter[type]=' + type : '';
    if (params && params[0] === '&') params = params.substr(1);
    this.apiRequest('GET', '/api/posts', params, onSuccess, onFailure);
  }

  /**
   * POST /api/posts - create a new post
   *
   *   - discussionId: int
   *   - content: string
   */
  createPost(discussionId, content, onSuccess, onFailure) {
    var attributes = {"content": content};
    var relationships = {"discussion": {"data":
      {"type": "discussions", "id": discussionId}}};
    var dataStr = Flarum._genData('posts', '', attributes, relationships);
    this.apiRequest('POST', '/api/posts', dataStr,
        onSuccess, onFailure);
  }

  /**
   * GET /api/posts/:id - get a post by ID
   */
  getPostById(postId, onSuccess, onFailure) {
    var path = '/api/posts/' + postId;
    this.apiRequest('GET', path, '', onSuccess, onFailure);
  }

  /**
   * PATCH /api/posts/:id - update a post
   *
   *   - postId: int
   *   - content: string
   */
  updatePost(postId, content, onSuccess, onFailure) {
    var path = '/api/posts/' + postId;
    var dataStr = Flarum._genData('posts', postId, {'content': content}, undefined);
    this.apiRequest('PATCH', path, dataStr, onSuccess, onFailure);
  }
  /**
   * PATCH /api/posts/:id - approve a reply
   *
   *   - approved: bool
   *   - hidden: bool
   */
  approveReply(postId, approved, hidden, onSuccess, onFailure) {
    var path = '/api/posts/' + postId;
    var dataStr = Flarum._genData('posts', postId, {
        "isApproved": Boolean(approved),
        "isHidden": Boolean(hidden)
        }, undefined);
    this.apiRequest('PATCH', path, dataStr, onSuccess, onFailure);
  }

  /**
   * DELETE /api/posts/:id - delete a post
   */
  deletePost(postId, onSuccess, onFailure) {
    var path = '/api/posts/' + postId;
    this.apiRequest('DELETE', path, '', onSuccess, onFailure);
  }


  // Users
  /**
   * GET /api/users - get all users
   *
   *     filter[q] - filter by username/gambits
   */
  getUsers(q, onSuccess, onFailure) {
    var params = q ? 'filter[q]=' + q : '';
    this.apiRequest('GET', '/api/users', params, onSuccess, onFailure);
  }

  /**
   * POST /api/users - register a new user
   *
   *   - name: string
   *   - email: string
   *   - passwd: string
   */
  createUser(name, email, passwd, onSuccess, onFailure) {
    var user = {
      "username": name,
      "email": email,
      "password": passwd
    };
    var dataStr = Flarum._genData('users', '', user, undefined);
    this.apiRequest('POST', '/api/users', dataStr, onSuccess, onFailure);
  }

  /**
   * GET /api/users/:idOrUsername - get a user by ID or username
   *
   */
  getUser(userId, onSuccess, onFailure) {
    this.apiRequest('GET', '/api/users/' + userId, '', onSuccess, onFailure);
  }

  /**
   * PATCH /api/users/:id - update a user
   *
   *    - userId: int
   *    - preferences: object, e.g.
   *          {
   *              "notify_discussionRenamed_alert": true,
   *              "notify_postLiked_alert": true,
   *              "notify_discussionLocked_alert": true,
   *              "notify_postMentioned_alert": true,
   *              "notify_postMentioned_email": false,
   *              "notify_userMentioned_alert": true,
   *              "notify_userMentioned_email": false,
   *              "notify_newPost_alert": true,
   *              "notify_newPost_email": true,
   *              "followAfterReply": false,
   *              "discloseOnline": true,
   *              "indexProfile": true,
   *              "locale": null
   *          }
   */
  updateUser(userId, preferences, onSuccess, onFailure) {
    var path = '/api/users/' + userId;
    var dataStr = Flarum._genData('users', userId, {
        "preferences": preferences
        }, undefined);
    this.apiRequest('PATCH', path, dataStr, onSuccess, onFailure);
  }

  /**
   * DELETE /api/users/:id - delete a user
   *
   */
  deleteUser(userId, onSuccess, onFailure) {
    var path = '/api/users/' + userId;
    this.apiRequest('DELETE', path, '', onSuccess, onFailure);
  }

  /**
   * POST /api/users/:id/avatar - upload a user avatar
   *
   *   TODO data
   */
  uploadAvatar(userId, avatar, onSuccess, onFailure) {
    this.apiRequest('POST', '/api/users/' + userId + '/avatar', avatar, onSuccess, onFailure);
  }

  /**
   * DELETE /api/users/:id/avatar - delete a user avatar
   */
  deleteAvatar(userId, onSuccess, onFailure) {
    this.apiRequest('DELETE', '/api/users/' + userId + '/avatar', '', onSuccess, onFailure);
  }


  // Groups
  /**
   * GET /api/groups - get all groups
   *
   */
  getGroups(onSuccess, onFailure) {
    this.apiRequest('GET', '/api/groups', '', onSuccess, onFailure);
  }

  /**
   * POST /api/groups - create a new group
   *
   *    - nameSingular: string
   *    - namePlural: string
   *    - color: string (#000000), optional
   *    - icon: string (FontAwesome icon name), optional
   */
  createGroup(nameSingular, namePlural, color, icon,
      onSuccess, onFailure) {
    var dataStr = Flarum._genData('groups', '', {
        "nameSingular": nameSingular,
        "namePlural": namePlural,
        "color": color,
        "icon": icon
        }, undefined);
    this.apiRequest('POST', '/api/groups', dataStr, onSuccess, onFailure);
  }

  /**
   * PATCH /api/groups/:id - update a group
   *
   *    - groupId: int
   *    - nameSingular: string
   *    - namePlural: string
   *    - color: string (#000000)
   *    - icon: string (FontAwesome icon name)
   */
  updateGroup(groupId, nameSingular, namePlural, color, icon,
      onSuccess, onFailure) {
    var path = '/api/groups/' + groupId;
    var attributes: any = {};
    if (nameSingular) attributes.nameSingular = nameSingular;
    if (namePlural) attributes.namePlural = namePlural;
    if (color) attributes.color = color;
    if (icon) attributes.icon = icon;
    var dataStr = Flarum._genData('groups', groupId, attributes, undefined);
    this.apiRequest('PATCH', path, dataStr, onSuccess, onFailure);
  }

  /**
   * DELETE /api/groups/:id - delete a group
   *
   */
  deleteGroup(groupId, onSuccess, onFailure) {
    var path = '/api/groups/' + groupId;
    this.apiRequest('DELETE', path, '', onSuccess, onFailure);
  }


  // Notifications
  /**
   * GET /api/notifications - get all notifications
   *
   */
  getNotifications(onSuccess, onFailure) {
    this.apiRequest('GET', '/api/notifications', '', onSuccess, onFailure);
  }

  /**
   * PATCH /api/notifications/:id - mark a notification as read
   */
  markNotification(notificationId, onSuccess, onFailure) {
    var path = '/api/notifications/' + notificationId;
    var dataStr = Flarum._genData('notifications', notificationId, {
        "isRead": true
        }, undefined);
    this.apiRequest('PATCH', path, dataStr, onSuccess, onFailure);
  }
  /**
   * POST /api/notifications/read - mark all notifications as read
   */
  markAllNotifications(onSuccess, onFailure) {
    this.apiRequest('POST', '/api/notifications/read', '', onSuccess, onFailure);
  }


  // Tags
  /**
   * POST /api/tags - create a new tag
   *
   *   - name: string, required
   *   - slug: string
   *   - desc: string
   *   - color: string (#000000)
   */
  createTag(name, slug, desc, color, onSuccess, onFailure) {
    var dataStr = Flarum._genData('tags', '', {
        "name": name,
        "slug": slug || name.toLowerCase(),
        "description": desc || '',
        "color": color || '',
        "isHidden": false,
        }, undefined);
    this.apiRequest('POST', '/api/tags', dataStr, onSuccess, onFailure);
  }

  /**
   * PATCH /api/tags/:id - update a tag
   *
   *   - tagId: int
   *   - name: string
   *   - slug: string
   *   - desc: string
   *   - color: string (#000000)
   */
  updateTag(tagId, name, slug, desc, color, onSuccess, onFailure) {
    var path = '/api/tags/' + tagId;
    var attributes: any = {};
    if (name) attributes.name = name;
    if (slug) attributes.slug = slug;
    if (desc) attributes.desc = desc;
    if (color) attributes.color = color;
    var dataStr = Flarum._genData('tags', tagId, attributes, undefined);
    this.apiRequest('PATCH', path, dataStr, onSuccess, onFailure);
  }

  /**
   * DELETE /api/tags/:id - delete a tag
   */
  deleteTag(tagId, onSuccess, onFailure) {
    var path = '/api/tags/' + tagId;
    this.apiRequest('DELETE', path, '', onSuccess, onFailure);
  }

}

export var flarum: Flarum = new Flarum('https://pl.zjut.party');

export interface FResource {
  id: string,
  type: string,
  attributes: any,
  relationships: any
}
